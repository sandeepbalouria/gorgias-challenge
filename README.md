# gorgias-challenge

Three Directories :
1. app has a flask app 
2. helm has helm charts for replication enabled postgres and flask app
3. terraform has terraform code for deploying GKE cluster in the default VPC.

To deploy this app on GKE:
1. docker build/push in app directory
2. terraform apply in terraform directory(after setting up gcp serviceaccount and permissions)
3. helm install postgres && helm install flask-app in helm directory

