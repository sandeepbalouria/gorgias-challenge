from flask import Flask, render_template, request, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__)

def get_env_variable(name):
    try:
        return os.environ[name]
    except KeyError:
        message = "Expected environment variable '{}' not set.".format(name)
        raise Exception(message)

# the values of those depend on your setup
PRIMARY_DB = get_env_variable("PRIMARY_DB")
REPLICA_DB = get_env_variable("REPLICA_DB")
####### Database#######

DB_URL = 'postgresql+psycopg2://postgres:postgres@{host}:5432/postgres'.format(host=PRIMARY_DB)
SQLALCHEMY_BINDS = {
    'read': 'postgresql+psycopg2://postgres:postgres@{host}:5432/postgres'.format(host=REPLICA_DB),
    'master': 'postgresql+psycopg2://postgres:postgres@{host}:5432/postgres'.format(host=PRIMARY_DB)
}
app.config['SQLALCHEMY_DATABASE_URI'] = DB_URL
app.config['SQLALCHEMY_BINDS'] = SQLALCHEMY_BINDS
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

class TodoRead(db.Model):
    __tablename__ = 'todo'
    __bind_key__ = 'read'
    id = db.Column(db.Integer, primary_key = True)
    text = db.Column(db.String(200))
    complete = db.Column(db.Boolean)

class Todo(db.Model):
    __tablename__ = 'todo'
    __bind_key__ = 'master'
    __table_args__ = {'extend_existing': True}
    id = db.Column(db.Integer, primary_key = True)
    text = db.Column(db.String(200))
    complete = db.Column(db.Boolean)

###### Route when nothing is specified in the url######
@app.route('/')
def index():
    incomplete = TodoRead.query.filter_by(complete = False).all()
    complete = TodoRead.query.filter_by(complete = True).all()

    return render_template('index.html',
       incomplete = incomplete, complete = complete)

###### Adding items######
@app.route('/add', methods =['POST'])
def add():
    todo = Todo(text = request.form['todoitem'], complete = False)
    db.session.add(todo)
    db.session.commit()

###### Makes to stay on the same home page######
    return redirect(url_for('index'))

###### Complete items######
@app.route('/complete/<id>')
def complete(id):

    todo = Todo.query.filter_by(id = int(id)).first()
    todo.complete = True
    db.session.commit()
    ###### Makes to stay on the same home page######

    return redirect(url_for('index'))


if __name__ == "__main__":
    db.create_all(bind='master')
    app.run(host='0.0.0.0', port=5000, debug=True, threaded=True)

