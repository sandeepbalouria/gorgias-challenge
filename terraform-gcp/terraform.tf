terraform {
  backend "gcs" {
    credentials = "~/keys/terraform-gke-gorgias-keyfile.json"
    bucket      = "terraform-gke-gorgias-sandeep"
    prefix      = "terraform/state"
  }
}
